#! /bin/sh
### BEGIN INIT INFO
# Provides:          fshare
# Required-Start:    mountkernfs
# Required-Stop:
# Should-Start:
# X-Start-Before:
# Default-Start:     S
# Default-Stop:
# Short-Description: FShare HTTP server
# Description: FShare HTTP server
### END INIT INFO

if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"
    INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi

DESC="FShare"
FSHARE_HOME=/var/www/fshare
DAEMON=${FSHARE_HOME}/fshare/fshare.py
PIDFILE=/run/fshare.pid
USER=fshare
GROUP=nogroup
#ARGS="-d"
ARGS=""
STATE_FILE=${FSHARE_HOME}/state
DATA_DIR=${FSHARE_HOME}/data

do_start_cmd() {
    start-stop-daemon --start --oknodo --pidfile ${PIDFILE} --user ${USER} \
            --background --make-pidfile --chuid ${USER}:${GROUP} \
            --startas ${DAEMON} -- ${ARGS} ${STATE_FILE} "${DATA_DIR}"
    return $?
}

do_stop() {
    start-stop-daemon --stop --oknodo --pidfile ${PIDFILE} --user ${USER}
    return $?
}
do_status() {
    status_of_proc -p ${PIDFILE} ${DAEMON} ${DESC}
    return $?
}
